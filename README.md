## Demo & Deployment

You can access to this video & deployment URL:
1. Video Demo: `https://youtu.be/D5DkUj6GGH0`
2. Deployment Site: `https://briix-afif.netlify.app`

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```