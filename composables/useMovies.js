import { uuid } from 'vue-uuid';

export default function useMovies() {
  const movies = useState('movies', () => [{
    id: uuid.v4(),
    title: 'The Batman',
    director: 'Matt Reeves',
    summary: 'The Batman is a 2022 American superhero film based on the DC Comics character Batman.',
    genres: 'Action',
  }, {
    id: uuid.v4(),
    title: 'Frozen',
    director: 'Chris Buck',
    summary: 'Frozen is a 2013 American computer-animated musical fantasy film produced by Walt Disney Animation Studios.',
    genres: 'Animation',
  },
  {
    id: uuid.v4(),
    title: 'Power Ranger',
    director: 'Dean Israel',
    summary: 'Power Ranger is a 2017 American computer-animated superhero film.',
    genres: 'Sci-Fi',
  },
  {
    id: uuid.v4(),
    title: 'The Conjuring',
    director: 'James Wan',
    summary: 'Conjuring is a 2013 American horror film directed by James Wan.',
    genres: 'Horror',
  },
  {
    id: uuid.v4(),
    title: 'The Notebook',
    director: 'Nick Cassavetes',
    summary: 'The Notebook is a 2004 American romantic drama film directed by Nick Cassavetes.',
    genres: 'Drama',
  }]);

  const addMovie = (movie) => {
    movies.value = [...movies.value, { id: uuid.v4(), ...movie }];
  }

  const deleteMovie = (id) => {
    movies.value = movies.value.filter(m => m.id !== id);
  }

  const updateMovie = (movie) => {
    const updatedMovie = movies.value.map(m =>
      m.id === movie.id ?
        { ...movies.value, ...movie } : m);

    movies.value = updatedMovie
  }

  return {
    movies,
    addMovie,
    deleteMovie,
    updateMovie
  }
}